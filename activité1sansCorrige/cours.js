// Question 1 : écrire la fonction compterElements

	function compterElements(selection){

	// ecrire le code ici
		var num = 0;
		
		if(selection == "first"){
			num = document.querySelectorAll('a')[0].href;
		}
		else if(selection == "last"){
			num = document.querySelectorAll('a')[document.querySelectorAll('a').length-1].href;
		}

		else{
			num = document.querySelectorAll(selection).length;
		}


		return num;
		
	}

console.log(compterElements("section"));  //4
console.log(compterElements("#footer-area .footer-segment")); //4
console.log(compterElements("#sidebar a")); //6

// Question 2: 

//afficher :
//le nombre total de liens dans la page web ;
//la cible du premier et du dernier lien.

console.log(compterElements("a")); //22
console.log(compterElements("first")); //file:///C:/Users/Admin/Desktop/WEB/projettp/Acceuil.html
console.log(compterElements("last")); // file:///C:/Users/Admin/Desktop/WEB/projettp/fin.html

//Question 3:

//Modifiez le fichier cours.js afin d'ajouter sous la liste Liens importants un paragraphe contenant 
//un lien vers l'URL suivante : https://fr.wikipedia.org/wiki/Liste_des_langages_de_programmation. 
//voir le fichier index.html

function creerLien(link , location, nom){

	var lien = document.createElement('li');
	 lien.innerHTML= '<a href ="'+link+'"> '+nom+' <a/>';
	 if(location == '#sidebar ul'){
		document.querySelector('#sidebar ul').appendChild(lien);
	}else{
		document.querySelector('#sidebar .follow_us ul').appendChild(lien);
	}
	console.log(location);
}

creerLien("www.fr.wikipedia.org/wiki/Liste_des_langages_de_programmation",'#sidebar ul', 'Do stuff');
//Question 4 

//avec le code initial ci-dessous, qui définit un tableau de chaînes de caractères.
// Liste de nos pages web 
var contact = ["www.Facebook.fr/compagnie", "www.Twitter/compagnie", "www.Google+/compagnie"];

// TODO : ajouter la liste de ces pages, dans la div "asides" Nous rejoindre

// Complétez ce fichier afin d'ajouter la liste des liens sur la page web.
// Vous devez utiliser le tableau  contact et chaque lien doit être cliquable.﻿﻿
//changez la taille au besoin 

function asideCreation(contact){
	for (var i = 0; i <contact.length; i++) {
		creerLien(contact[i],'#sidebar .follow_us ul', 'lien creer n '+(i+1));
	}
}

asideCreation(contact);

//Question 5 

/*ajouter du code qui permet de modifier la couleur de la bordure des quatre éléments div 
lorsque l'utilisateur appuie sur la touche S qui indique la grande section (rouge), H qui indique header 
(jaune), F qui indique footer (vert) ,N qui indique nav (noir) , a aside en (bleu). 
 */


// utiliser cette indication 
/*
// Gestion de l'appui sur une touche du clavier produisant un caractère
document.addEventListener("keypress", function (e) {
    console.log("Vous avez appuyé sur la touche " + String.fromCharCode(e.charCode));
});
*/

document.addEventListener("keypress", function (e) {
       console.log("Vous avez appuyé sur la touche " + String.fromCharCode(e.charCode));

       switch(String.fromCharCode(e.charCode)){
			case 's':
			document.querySelector('#main').style.border = "solid 5px #D50000";
				break;
			case 'h':
			document.querySelector('header').style.border = "solid 5px #FFEB3B";
				break;
			case 'f':
			document.querySelector('#footer-area').style.border = "solid 5px #009688";
				break;
			case 'n':
			document.querySelector('.menu').style.border = "solid 5px #000000";
				break;
       }
});